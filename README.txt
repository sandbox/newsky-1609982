Feed Date Changed add the possibility to import the changed date of entities with the module Feeds.

When you import data with Feeds, changed date of the entity is defined with the current timestamp when importing.

With Feeds you can import the published date of the entity. Sometimes it is also important to be able to import the changed date of the entity.

Whith Feed Date Changed you will have available a new target when mapping fields that allow you to import the changed date in your feed.